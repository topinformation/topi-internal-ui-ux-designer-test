# UI/UX Designer Test #


### Goal ###

* Create a new design for the 2 mobile screens below
* Propose a new experience, without using the side menu
* Create the minimal flow in Figma or Adobe XD
* Send the result to the recruiter

Reference layout:

![Alt text](https://dl.dropbox.com/s/qxlpwqklozjibz3/MO03%20-%20Menu%20lateral.png)
![Alt text](https://dl.dropbox.com/s/e0d20kc5nfay15j/MO04%20-%20Or%C3%A7amento.png)
